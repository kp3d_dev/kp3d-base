![-logo goes here-](https://i.imgur.com/hBxqu6R.png)

The base engine. The map editor among other utilities are stored in separate respositories. I should warn you that the code is probably very sloppy. I'm not the best programmer in the world, but feel free to suggest possible improvements. I'm totally open to criticism.  

I've setup a wiki which may or may not have details explaining how certain parts of the engine work. I don't really know what's standard or what isn't when it comes to large scale code like this, I just write what makes sense to me.
# License

I'm still actively developing this project, so I haven't made a final decision on a license yet.  
For my WIP code (so the majority of this codebase) though, let's just assume I'm using
zlib/libpng.  

```
	Copyright (c) 2017-2018 KMP

	This software is provided 'as-is', without any express or implied
	warranty. In no event will the authors be held liable for any damages
	arising from the use of this software.

	Permission is granted to anyone to use this software for any purpose,
	including commercial applications, and to alter it and redistribute it
	freely, subject to the following restrictions:

	1. The origin of this software must not be misrepresented; you must not
	   claim that you wrote the original software. If you use this software
	   in a product, an acknowledgment in the product documentation would be
	   appreciated but is not required.
	
	2. Altered source versions must be plainly marked as such, and must not be
	   misrepresented as being the original software.
	
	3. This notice may not be removed or altered from any source distribution.
```

![Give me your toes](https://i.imgur.com/N61wi6V.png)

Give me your toes.