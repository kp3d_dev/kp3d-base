#pragma once

#include <string>
#include <unordered_map>
#include <fstream>

#include "Common.h"
#include "INonCopyable.h"

namespace kp3d
{
	/*
	 * Read-only settings/config loader
	 * Needs expanding in the future to modify settings/update the file upon destruction
	 */
	class Settings: private INonCopyable
	{
	public:
		Settings(const std::string& path);

		template <typename T>
		auto Get(const std::string& key) const;

	private:
		std::unordered_map<std::string, std::string> m_settings;
		std::ifstream m_file;

	};
}

#include "Settings.inl"
