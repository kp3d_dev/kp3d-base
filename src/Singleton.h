#pragma once

#include <assert.h>

#include "Common.h"
#include "INonCopyable.h"

namespace kp3d
{
	/*
	 * A basic singleton
	 *
	 * Caveats:
	 * - The singleton must be constructed explicitly
	 * - It's a template, so it must be instantiated with the type
	 *   that you're making a singleton out of
	 */
	template <typename T>
	class Singleton: private INonCopyable
	{
	public:
		Singleton()
		{
			assert(!ms_singleton);

			ms_singleton = static_cast<T*>(this);
		}

		virtual ~Singleton()
		{
			assert(ms_singleton);

			ms_singleton = nullptr;
		}

		static T& Instance()
		{
			assert(ms_singleton);

			return *ms_singleton;
		}

		static T* InstancePtr()
		{
			return ms_singleton;
		}

	protected:
		static T* ms_singleton;

	};
}
