#pragma once

#include <string>

#include <GL/glew.h>

#include "Common.h"
#include "INonCopyable.h"

namespace kp3d
{
	class Texture: private INonCopyable
	{
	public:
		Texture();
		~Texture();

		ErrCode Load(const std::string& path);
		ErrCode Load(const byte* data, uint width, uint height);

		GLuint GLID() const;

	private:
		GLuint m_gl_id;
		uint m_width;
		uint m_height;

	};
}
