/*
 * Not a fancy or pretty logger by any means but it gets the job done (inefficiently), I'll replace it with something better down the road
 */
#include "LogSystem.h"

#include <time.h>

#include <chrono>
#include <iomanip>
#include <iostream>

namespace
{
	// You aren't a real programmer until you put a giant ASCII logo of your project name into the log file for street cred
	const std::string k_logo[] =
	{
		R"(  _  __ _____  ____   _____  )",
		R"( | |/ /|  __ \|___ \ |  __ \ )",
		R"( | ' / | |__) | __) || |  | |)",
		R"( |  <  |  ___/ |__ < | |  | |)",
		R"( | . \ | |     ___) || |__| |)",
		R"( |_|\_\|_|    |____/ |_____/ )",
		R"(                             )"
	};

	const std::string k_prefix[] =
	{
		"[INFO]: ",
		"[WARNING]: ",
		"[ERROR]: "
	};
}

namespace kp3d
{
	template<> LogSystem* Singleton<LogSystem>::ms_singleton = nullptr;

	LogSystem::LogSystem(const std::string& path):
		m_file(path),
		m_enabled(true)
	{
		if (!m_file)
			Log(LogLevel::WARNING, "Failed to create log file{}", path.empty() ? ", no path was specified." : ".");

		for (const std::string& logo: k_logo)
			Log(LogLevel::INFO, logo);

		Log(LogLevel::INFO, "==============================================");
		Log(LogLevel::INFO, "  Copyright (C) kpworld.xyz 2018");
		Log(LogLevel::INFO, "  Written by KPCFTSZ");
		Log(LogLevel::INFO, "==============================================");
		Log(LogLevel::INFO, "");
		Log(LogLevel::INFO, "Initializing log system...");
	}

	LogSystem::~LogSystem()
	{
		Log(LogLevel::INFO, "Destroying log system...");
	}

	void LogSystem::Log(LogLevel severity, const std::string& message, fmt::ArgList args)
	{
		if (!m_enabled)
			return;

		time_t now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		auto time = std::put_time(localtime(&now), "[%T] ");

		std::clog << time << k_prefix[static_cast<size_t>(severity)] << fmt::format(message, args) << std::endl;
		m_file << time << k_prefix[static_cast<size_t>(severity)] << fmt::format(message, args) << std::endl;
	}

	void LogSystem::SetEnabled(bool enabled)
	{
		if (!enabled)
			Log(LogLevel::INFO, "Logging has been disabled.");

		m_enabled = enabled;
	}

	LogSystem& LogSystem::Instance()
	{
		assert(ms_singleton);

		return *ms_singleton;
	}

	LogSystem* LogSystem::InstancePtr()
	{
		return ms_singleton;
	}
}
