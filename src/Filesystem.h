#pragma once

#include <string>

#include "Common.h"

namespace kp3d
{
	namespace fs
	{
		std::string LoadFile(const std::string& path);
	}
}
