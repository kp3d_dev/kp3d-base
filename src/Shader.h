#pragma once

#include <string>

#include <GL/glew.h>

#include "Common.h"
#include "INonCopyable.h"
#include "Filesystem.h"

namespace kp3d
{
	class Shader: private INonCopyable
	{
	public:
		Shader();
		~Shader();

		ErrCode Load(const std::string& vs_path, const std::string& fs_path);
		GLuint GLID() const;

		template <typename T, typename... Args>
		void SetUniform(const std::string& name, T value, Args&&... extra);

	private:
		ErrCode CompileShader(GLuint gl_type);
		ErrCode LinkProgram() const;

		GLint GetUniformLocation(const std::string& name) const;

	private:
		GLuint m_gl_id;
		GLuint m_gl_vs_id;
		GLuint m_gl_fs_id;
		std::string m_vs_src;
		std::string m_fs_src;

	};
}

#include "Shader.inl"
