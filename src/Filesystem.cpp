#include "Filesystem.h"

#include <fstream>
#include <streambuf>
#include <exception>

#include "LogSystem.h"

namespace kp3d
{
	namespace fs
	{
		std::string LoadFile(const std::string& path)
		{
			std::ifstream file(path);

			if (!file)
			{
				LogSystem::Instance().Log(LogLevel::WARNING, "Failed to load file: \"{}\"", path);

				return "INVALID FILE";
			}

			return std::string((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
		}
	}
}
