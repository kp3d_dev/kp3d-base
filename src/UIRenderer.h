#pragma once

#include "Common.h"
#include "INonCopyable.h"

namespace kp3d
{
	class UIRenderer : private INonCopyable
	{
	public:
		UIRenderer();
		~UIRenderer();

		void Update();
		void Render();

	};
}
