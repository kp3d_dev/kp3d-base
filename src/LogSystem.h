#pragma once

#include <string>
#include <fstream>

#include <fmt/format.h>

#include "Common.h"
#include "INonCopyable.h"
#include "Singleton.h"

namespace kp3d
{
	enum class LogLevel
	{
		INFO,
		WARNING,
		ERROR
	};

	class LogSystem: public Singleton<LogSystem>
	{
	public:
		LogSystem(const std::string& path);
		~LogSystem();

		void Log(LogLevel severity, const std::string& message, fmt::ArgList args);
		FMT_VARIADIC(void, Log, LogLevel, const std::string&)

		void SetEnabled(bool enabled);

		static LogSystem& Instance();
		static LogSystem* InstancePtr();

	private:
		std::ofstream m_file;
		bool m_enabled;

	};
}
