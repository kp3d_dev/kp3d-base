#include "Quaternion.h"

#include <math.h>

#include <tuple>

namespace kp3d
{
	Quaternion::Quaternion():
		x(0.0f),
		y(0.0f),
		z(0.0f),
		w(0.0f)
	{
	}

	Quaternion::Quaternion(float x, float y, float z, float w):
		x(x),
		y(y),
		z(z),
		w(w)
	{
	}

	Quaternion::Quaternion(float scalar):
		x(scalar),
		y(scalar),
		z(scalar),
		w(scalar)
	{
	}

	Quaternion::Quaternion(const Vec3& axis, float angle)
	{
		float sin_half = sinf(angle / 2.0f);

		x = axis.x * sin_half;
		y = axis.y * sin_half;
		z = axis.z * sin_half;
		w = cosf(angle / 2.0f);
	}

	float Quaternion::Length() const
	{
		return sqrtf(x * x + y * y + z * z + w * w);
	}

	float Quaternion::Dot(const Quaternion& other) const
	{
		return x * other.x + y * other.y + z * other.z + w * other.w;
	}

	Quaternion Quaternion::Conjugate() const
	{
		return Quaternion(-x, -y, -z, w);
	}

	Quaternion Quaternion::Normalized() const
	{
		float length = Length();

		return Quaternion(x / length, y / length, z / length, w / length);
	}

	Quaternion& Quaternion::operator*=(const Quaternion& rhs)
	{
		x *= x * rhs.w + w * rhs.x + y * rhs.z - z * rhs.y;
		y *= y * rhs.w + w * rhs.y + z * rhs.x - x * rhs.z;
		z *= z * rhs.w + w * rhs.z + x * rhs.y - y * rhs.x;
		w *= w * rhs.w - x * rhs.x - y * rhs.y - z * rhs.z;

		return *this;
	}

	Quaternion& Quaternion::operator*=(const Vec3& rhs)
	{
		x *=  w * rhs.x + y * rhs.z - z * rhs.y;
		y *=  w * rhs.y + z * rhs.x - x * rhs.z;
		z *=  w * rhs.z + x * rhs.y - y * rhs.x;
		w *= -x * rhs.x - y * rhs.y - z * rhs.x;

		return *this;
	}

	Quaternion Quaternion::operator-() const
	{
		return Quaternion(-x, -y, -z, -w);
	}

	bool operator==(const Quaternion& lhs, const Quaternion& rhs)
	{
		return std::tie(lhs.x, lhs.y, lhs.z, lhs.w) == std::tie(rhs.x, rhs.y, rhs.z, rhs.w);
	}

	bool operator!=(const Quaternion& lhs, const Quaternion& rhs)
	{
		return !(lhs == rhs);
	}

	bool operator< (const Quaternion& lhs, const Quaternion& rhs)
	{
		return std::tie(lhs.x, lhs.y, lhs.z, lhs.w) < std::tie(rhs.x, rhs.y, rhs.z, rhs.w);
	}

	bool operator<=(const Quaternion& lhs, const Quaternion& rhs)
	{
		return !(rhs < lhs);
	}

	bool operator> (const Quaternion& lhs, const Quaternion& rhs)
	{
		return rhs < lhs;
	}

	bool operator>=(const Quaternion& lhs, const Quaternion& rhs)
	{
		return !(lhs < rhs);
	}

	Quaternion operator*(const Quaternion& lhs, const Quaternion& rhs)
	{
		return Quaternion(lhs) *= rhs;
	}

	Quaternion operator*(const Quaternion& lhs, const Vec3& rhs)
	{
		return Quaternion(lhs) *= rhs;
	}

	Quaternion operator*(const Vec3& lhs, const Quaternion& rhs)
	{
		return Quaternion(rhs) *= lhs;
	}

	std::ostream& operator<<(std::ostream& lhs, const Quaternion& rhs)
	{
		lhs << "{" << rhs.x << ", " << rhs.y << ", " << rhs.z << ", " << rhs.w << "}";

		return lhs;
	}
}