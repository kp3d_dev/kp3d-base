#include "Shader.h"

#include "LogSystem.h"

namespace kp3d
{
	Shader::Shader():
		m_gl_id(glCreateProgram()),
		m_gl_vs_id(0),
		m_gl_fs_id(0)
	{
	}

	Shader::~Shader()
	{
		glDeleteProgram(m_gl_id);
	}

	ErrCode Shader::Load(const std::string& vs_path, const std::string& fs_path)
	{
		m_vs_src = const_cast<GLchar*>(fs::LoadFile(vs_path).c_str());
		m_fs_src = const_cast<GLchar*>(fs::LoadFile(fs_path).c_str());

		return static_cast<ErrCode>(
			CompileShader(GL_VERTEX_SHADER) |
			CompileShader(GL_FRAGMENT_SHADER) |
			LinkProgram()
		);
	}

	GLuint Shader::GLID() const
	{
		return m_gl_id;
	}

	//

	ErrCode Shader::CompileShader(GLuint gl_type)
	{
		const GLchar* gl_src = gl_type == GL_VERTEX_SHADER ? m_vs_src.c_str() : m_fs_src.c_str();
		GLuint& gl_id = gl_type == GL_VERTEX_SHADER ? m_gl_vs_id : m_gl_fs_id;
		GLint gl_error_code = 0;
		GLchar gl_error_log[KP3D_BUFFER_SIZE];

		std::fill_n(gl_error_log, KP3D_BUFFER_SIZE, 0);

		gl_id = glCreateShader(gl_type);
		glShaderSource(gl_id, 1, &gl_src, NULL);
		glCompileShader(gl_id);
		glGetShaderiv(gl_id, GL_COMPILE_STATUS, &gl_error_code);

		if (!gl_error_code)
		{
			glGetShaderInfoLog(gl_id, KP3D_BUFFER_SIZE, NULL, gl_error_log);
			LogSystem::Instance().Log(LogLevel::INFO, "Failed to compile {} shader:\n{}", gl_type == GL_VERTEX_SHADER ? "vertex" : "fragment", gl_error_log);

			return ErrCode::FAILURE;
		}

		glAttachShader(m_gl_id, gl_id);

		return ErrCode::SUCCESS;
	}

	ErrCode Shader::LinkProgram() const
	{
		GLint gl_error_code;
		GLchar gl_error_log[KP3D_BUFFER_SIZE];

		std::fill_n(gl_error_log, KP3D_BUFFER_SIZE, 0);

		glLinkProgram(m_gl_id);
		glGetProgramiv(m_gl_id, GL_LINK_STATUS, &gl_error_code);

		if (!gl_error_code)
		{
			glGetProgramInfoLog(m_gl_id, KP3D_BUFFER_SIZE, NULL, gl_error_log);
			LogSystem::Instance().Log(LogLevel::INFO, "Failed to link shader program:\n{}", gl_error_log);

			return ErrCode::FAILURE;
		}

		glValidateProgram(m_gl_id);
		glGetProgramiv(m_gl_id, GL_VALIDATE_STATUS, &gl_error_code);

		if (!gl_error_code)
		{
			glGetProgramInfoLog(m_gl_id, KP3D_BUFFER_SIZE, NULL, gl_error_log);
			LogSystem::Instance().Log(LogLevel::INFO, "Failed to validate shader program:\n{}", gl_error_log);

			return ErrCode::FAILURE;
		}

		glDeleteShader(m_gl_vs_id);
		glDeleteShader(m_gl_fs_id);

		return ErrCode::SUCCESS;
	}

	GLint Shader::GetUniformLocation(const std::string& name) const
	{
		return glGetUniformLocation(m_gl_id, name.c_str());
	}
}
