#pragma once

#include <memory>
#include <string>

#include <SDL2/SDL.h>

#include "Common.h"
#include "INonCopyable.h"
#include "LogSystem.h"
#include "UIRenderer.h"
#include "Settings.h"

namespace kp3d
{
	class KP3D: private INonCopyable
	{
	public:
		KP3D(const std::string& config_path);
		~KP3D();
		void Run();

	private:
		void Update();
		void Render();

	private:
		// Engine subsystems and third-party libraries, ordered in reverse destruction sequence
		std::unique_ptr<LogSystem> m_log_system;
		std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> m_sdl_window;
		std::unique_ptr<void, decltype(&SDL_GL_DeleteContext)> m_sdl_context;
		std::unique_ptr<UIRenderer> m_ui_renderer;
		
		// Misc.
		bool m_running;
		Settings m_game_config;

	};
}
