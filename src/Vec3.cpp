#include "Vec3.h"

#include <math.h>

#include <tuple>

#include "MathUtil.h"

namespace kp3d
{
	Vec3::Vec3():
		x(0.0f),
		y(0.0f),
		z(0.0f)
	{
	}

	Vec3::Vec3(float x, float y, float z):
		x(x),
		y(y),
		z(z)
	{
	}

	Vec3::Vec3(float scalar):
		x(scalar),
		y(scalar),
		z(scalar)
	{
	}

	Vec3::Vec3(const Quaternion& quaternion):
		x(quaternion.x),
		y(quaternion.y),
		z(quaternion.z)
	{
	}

	float Vec3::Length() const
	{
		return sqrtf(x * x + y * y + z * z);
	}

	float Vec3::Dot(const Vec3& other) const
	{
		return x * other.x + y * other.y + z * other.z;
	}

	Vec3 Vec3::Cross(const Vec3& other) const
	{
		return Vec3(
			y * other.z - z * other.y,
			z * other.x - x * other.z,
			x * other.y - y * other.x
		);
	}

	Vec3 Vec3::Normalized() const
	{
		float length = Length();

		return Vec3(x / length, y / length, z / length);
	}

	Vec3 Vec3::Rotated(const Vec3& axis, float degrees) const
	{
		float radians = -ToRadians(degrees);
		float sin = sinf(radians);
		float cos = cosf(radians);

		return Cross(axis * sin) + (*this * cos) + (axis * Dot(axis * (1.0f - cos)));
	}

	Vec3 Vec3::Rotated(const Quaternion& rotation) const
	{
		return Vec3((*this * rotation) * rotation.Conjugate());
	}

	Vec3& Vec3::operator+=(const Vec3& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		z += rhs.z;

		return *this;
	}

	Vec3& Vec3::operator+=(float rhs)
	{
		x += rhs;
		y += rhs;
		z += rhs;

		return *this;
	}

	Vec3& Vec3::operator-=(const Vec3& rhs)
	{
		x -= rhs.x;
		y -= rhs.y;
		z -= rhs.z;

		return *this;
	}

	Vec3& Vec3::operator-=(float rhs)
	{
		x -= rhs;
		y -= rhs;
		z -= rhs;

		return *this;
	}

	Vec3& Vec3::operator*=(const Vec3& rhs)
	{
		x *= rhs.x;
		y *= rhs.y;
		z *= rhs.z;

		return *this;
	}

	Vec3& Vec3::operator*=(float rhs)
	{
		x *= rhs;
		y *= rhs;
		z *= rhs;

		return *this;
	}

	Vec3& Vec3::operator/=(const Vec3& rhs)
	{
		x /= rhs.x;
		y /= rhs.y;
		z /= rhs.z;

		return *this;
	}

	Vec3& Vec3::operator/=(float rhs)
	{
		x /= rhs;
		y /= rhs;
		z /= rhs;

		return *this;
	}

	Vec3 Vec3::operator-() const
	{
		return Vec3(-x, -y, -z);
	}

	bool operator==(const Vec3& lhs, const Vec3& rhs)
	{
		return std::tie(lhs.x, lhs.y, lhs.z) == std::tie(rhs.x, rhs.y, rhs.z);
	}

	bool operator!=(const Vec3& lhs, const Vec3& rhs)
	{
		return !(lhs == rhs);
	}

	bool operator< (const Vec3& lhs, const Vec3& rhs)
	{
		return std::tie(lhs.x, lhs.y, lhs.z) < std::tie(rhs.x, rhs.y, rhs.z);
	}

	bool operator<=(const Vec3& lhs, const Vec3& rhs)
	{
		return !(rhs < lhs);
	}

	bool operator> (const Vec3& lhs, const Vec3& rhs)
	{
		return rhs < lhs;
	}

	bool operator>=(const Vec3& lhs, const Vec3& rhs)
	{
		return !(lhs < rhs);
	}

	Vec3 operator+(const Vec3& lhs, const Vec3& rhs)
	{
		return Vec3(lhs) += rhs;
	}

	Vec3 operator-(const Vec3& lhs, const Vec3& rhs)
	{
		return Vec3(lhs) -= rhs;
	}

	Vec3 operator*(const Vec3& lhs, const Vec3& rhs)
	{
		return Vec3(lhs) *= rhs;
	}

	Vec3 operator/(const Vec3& lhs, const Vec3& rhs)
	{
		return Vec3(lhs) /= rhs;
	}

	Vec3 operator+(const Vec3& lhs, float rhs)
	{
		return Vec3(lhs) += rhs;
	}

	Vec3 operator+(float lhs, const Vec3& rhs)
	{
		return Vec3(rhs) += lhs;
	}

	Vec3 operator-(const Vec3& lhs, float rhs)
	{
		return Vec3(lhs) -= rhs;
	}

	Vec3 operator-(float lhs, const Vec3& rhs)
	{
		return Vec3(rhs) -= lhs;
	}

	Vec3 operator*(const Vec3& lhs, float rhs)
	{
		return Vec3(lhs) *= rhs;
	}

	Vec3 operator*(float lhs, const Vec3& rhs)
	{
		return Vec3(rhs) *= lhs;
	}

	Vec3 operator/(const Vec3& lhs, float rhs)
	{
		return Vec3(lhs) /= rhs;
	}

	Vec3 operator/(float lhs, const Vec3& rhs)
	{
		return Vec3(rhs) /= lhs;
	}

	std::ostream& operator<<(std::ostream& lhs, const Vec3& rhs)
	{
		lhs << "{" << rhs.x << ", " << rhs.y << ", " << rhs.z << "}";

		return lhs;
	}
}
