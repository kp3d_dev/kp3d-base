#include "UIRenderer.h"

#include "LogSystem.h"

namespace kp3d
{
	UIRenderer::UIRenderer()
	{
		LogSystem::Instance().Log(LogLevel::INFO, "Initializing UI renderer...");
	}

	UIRenderer::~UIRenderer()
	{
		LogSystem::Instance().Log(LogLevel::INFO, "Destroying UI renderer...");
	}

	void UIRenderer::Update()
	{
		// ...
	}

	void UIRenderer::Render()
	{
		// ...
	}
}