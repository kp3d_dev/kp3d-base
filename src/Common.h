#pragma once

// Platform detection
#if defined(_WIN32)
	#define KP3D_PLATFORM_WINDOWS
	#define NOMINMAX
	#define WIN32_LEAN_AND_MEAN
	#pragma warning(disable: 4018) // signed/unsigned mismatch
	#pragma warning(disable: 4996) // muh secure function warnings
#elif defined(__APPLE__) && defined(__MACH__)
	#include "TargetConditionals.h"
	#if TARGET_OS_MAC
		#define KP3D_PLATFORM_MAC
	#endif
#elif defined(__linux__)
	#define KP3D_PLATFORM_LINUX
#elif defined(__FreeBSD__) || defined(__OpenBSD__)
	#define KP3D_PLATFORM_BSD
#else
	#error Failed to detect supported platform
#endif

// Compiler detection
#if defined(__clang__)
	#define KP3D_CLANG
#elif defined(__GNUC__) || defined(__GNUG__)
	#define KP3D_GCC
	#if defined(__MINGW32__)
		#define KP3D_MINGW
	#endif
#elif defined(_MSC_VER)
	#define KP3D_MSVC
#endif

// Forced inlining
#if defined(KP3D_MSVC)
	#define KP3D_INLINE __forceinline
#else
	#define KP3D_INLINE __attribute__((always_inline))
#endif

// Util macros
#define KP3D_BUFFER_SIZE 4096

namespace kp3d
{
	// Basic types
	using uint8 = unsigned char;
	using uint16 = unsigned short;
	using uint32 = unsigned int;
	using uint64 = unsigned long long;

	using int8 = signed char;
	using int16 = signed short;
	using int32 = signed int;
	using int64 = signed long long;

	using uint = uint32;
	using byte = uint8;

	// Error handling
	enum ErrCode
	{
		SUCCESS, FAILURE
	};
}
