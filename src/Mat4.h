#pragma once

#include <array>

#include "Common.h"
#include "Vec3.h"

namespace kp3d
{
	class Mat4
	{
	public:
		Mat4(std::array<float, 16> mat);
		Mat4();

		Mat4& InitIdentity();
		Mat4& InitTranslation(const Vec3& translation);
		Mat4& InitRotation(const Vec3& rotation);
		Mat4& InitScale(const Vec3& scale);
		Mat4& InitPerspective(float fov, float aspect, float near = 0.01f, float far = 1000.0f);
		Mat4& InitOrthographic(float left, float right, float bottom, float top, float near = 0.0f, float far = 1.0f);

		friend Mat4 operator*(const Mat4& lhs, const Mat4& rhs);

	public:
		std::array<float, 16> mat;

	};
}
