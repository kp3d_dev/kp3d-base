#pragma once

#include <type_traits>

#include "Shader.h"
#include "Vec2.h"
#include "Vec3.h"
#include "Mat4.h"

namespace kp3d
{
	template <typename T, typename... Args>
	void Shader::SetUniform(const std::string& name, T value, Args&&... extra)
	{
		if constexpr (std::is_same_v<T, int>)
			glUniform1i(GetUniformLocation(name), value);
		else if constexpr (std::is_same_v<T, float>)
			glUniform1f(GetUniformLocation(name), value);
		else if constexpr (std::is_same_v<T, int*>)
			glUniform1iv(GetUniformLocation(name), extra[0], value);
		else if constexpr (std::is_same_v<T, float*>)
			glUniform1fv(GetUniformLocation(name), extra[0], value);
		else if constexpr (std::is_same_v<T, Vec2>)
			glUniform2f(GetUniformLocation(name), value.x, value.y);
		else if constexpr (std::is_same_v<T, Vec3>)
			glUniform3f(GetUniformLocation(name), value.x, value.y, value.z);
		else if constexpr (std::is_same_v<T, Mat4>)
			glUniformMatrix4fv(GetUniformLocation(name), 1, GL_FALSE, value.mat.data());
	}
}
