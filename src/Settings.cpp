#include "Settings.h"

#include <ctype.h>

#include <sstream>
#include <algorithm>

namespace
{
	std::string Trim(const std::string& str)
	{
		std::string trimmed = str;
		trimmed.erase(std::remove_if(trimmed.begin(), trimmed.end(), isspace), trimmed.end());

		return trimmed;
	}
}

namespace kp3d
{
	Settings::Settings(const std::string& path):
		m_file(path)
	{
		std::string line;

		while (std::getline(m_file, line))
		{
			std::istringstream input(line);
			std::string key;

			if (!line.empty() && Trim(line).at(0) == '#')
				continue;

			if (std::getline(input, key, '='))
			{
				std::string value;

				if (std::getline(input, value))
					m_settings.emplace(Trim(key), value);
			}
		}
	}
}
