#include "Vec2.h"

#include <math.h>

#include <tuple>

#include "MathUtil.h"

namespace kp3d
{
	Vec2::Vec2():
		x(0.0f),
		y(0.0f)
	{
	}

	Vec2::Vec2(float x, float y):
		x(x),
		y(y)
	{
	}

	Vec2::Vec2(float scalar):
		x(scalar),
		y(scalar)
	{
	}

	float Vec2::Length() const
	{
		return sqrtf(x * x + y * y);
	}

	float Vec2::Dot(const Vec2& other) const
	{
		return x * other.x + y * other.y;
	}

	float Vec2::Cross(const Vec2& other) const
	{
		return x * other.y - y * other.x;
	}

	Vec2 Vec2::Normalized() const
	{
		float length = Length();

		return Vec2(x / length, y / length);
	}

	Vec2 Vec2::Rotated(float degrees) const
	{
		float radians = ToRadians(degrees);
		float sin = sinf(radians);
		float cos = cosf(radians);

		return Vec2(x * cos - y * sin, x * sin + y * cos);
	}

	Vec2& Vec2::operator+=(const Vec2& rhs)
	{
		x += rhs.x;
		y += rhs.y;

		return *this;
	}

	Vec2& Vec2::operator+=(float rhs)
	{
		x += rhs;
		y += rhs;

		return *this;
	}

	Vec2& Vec2::operator-=(const Vec2& rhs)
	{
		x -= rhs.x;
		y -= rhs.y;

		return *this;
	}

	Vec2& Vec2::operator-=(float rhs)
	{
		x -= rhs;
		y -= rhs;

		return *this;
	}

	Vec2& Vec2::operator*=(const Vec2& rhs)
	{
		x *= rhs.x;
		y *= rhs.y;

		return *this;
	}

	Vec2& Vec2::operator*=(float rhs)
	{
		x *= rhs;
		y *= rhs;

		return *this;
	}

	Vec2& Vec2::operator/=(const Vec2& rhs)
	{
		x /= rhs.x;
		y /= rhs.y;

		return *this;
	}

	Vec2& Vec2::operator/=(float rhs)
	{
		x /= rhs;
		y /= rhs;

		return *this;
	}

	Vec2 Vec2::operator-() const
	{
		return Vec2(-x, -y);
	}

	bool operator==(const Vec2& lhs, const Vec2& rhs)
	{
		return std::tie(lhs.x, lhs.y) == std::tie(rhs.x, rhs.y);
	}

	bool operator!=(const Vec2& lhs, const Vec2& rhs)
	{
		return !(lhs == rhs);
	}

	bool operator< (const Vec2& lhs, const Vec2& rhs)
	{
		return std::tie(lhs.x, lhs.y) < std::tie(rhs.x, rhs.y);
	}

	bool operator<=(const Vec2& lhs, const Vec2& rhs)
	{
		return !(rhs < lhs);
	}

	bool operator> (const Vec2& lhs, const Vec2& rhs)
	{
		return rhs < lhs;
	}

	bool operator>=(const Vec2& lhs, const Vec2& rhs)
	{
		return !(lhs < rhs);
	}

	Vec2 operator+(const Vec2& lhs, const Vec2& rhs)
	{
		return Vec2(lhs) += rhs;
	}

	Vec2 operator-(const Vec2& lhs, const Vec2& rhs)
	{
		return Vec2(lhs) -= rhs;
	}

	Vec2 operator*(const Vec2& lhs, const Vec2& rhs)
	{
		return Vec2(lhs) *= rhs;
	}

	Vec2 operator/(const Vec2& lhs, const Vec2& rhs)
	{
		return Vec2(lhs) /= rhs;
	}

	Vec2 operator+(const Vec2& lhs, float rhs)
	{
		return Vec2(lhs) += rhs;
	}

	Vec2 operator+(float lhs, const Vec2& rhs)
	{
		return Vec2(rhs) += lhs;
	}

	Vec2 operator-(const Vec2& lhs, float rhs)
	{
		return Vec2(lhs) -= rhs;
	}

	Vec2 operator-(float lhs, const Vec2& rhs)
	{
		return Vec2(rhs) -= lhs;
	}

	Vec2 operator*(const Vec2& lhs, float rhs)
	{
		return Vec2(lhs) *= rhs;
	}

	Vec2 operator*(float lhs, const Vec2& rhs)
	{
		return Vec2(rhs) *= lhs;
	}

	Vec2 operator/(const Vec2& lhs, float rhs)
	{
		return Vec2(lhs) /= rhs;
	}

	Vec2 operator/(float lhs, const Vec2& rhs)
	{
		return Vec2(rhs) /= lhs;
	}

	std::ostream& operator<<(std::ostream& lhs, const Vec2& rhs)
	{
		lhs << "{" << rhs.x << ", " << rhs.y << "}";

		return lhs;
	}
}
