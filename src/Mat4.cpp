#include "Mat4.h"

#include <stddef.h>

namespace kp3d
{
	Mat4::Mat4(std::array<float, 16> mat):
		mat(mat)
	{
	}

	Mat4::Mat4():
		mat()
	{
	}

	Mat4& Mat4::InitIdentity()
	{
		for (size_t i = 0; i < 16; i++)
			mat[i] = (i & 3) == (i / 4) ? 1.0f : 0.0f;

		return *this;
	}

	Mat4& Mat4::InitTranslation(const Vec3& translation)
	{
		InitIdentity();

		mat[12] = translation.x;
		mat[13] = translation.y;
		mat[14] = translation.z;

		return *this;
	}

	Mat4& Mat4::InitRotation(const Vec3& rotation)
	{
		Mat4  x_rot = Mat4().InitIdentity();
		Mat4  y_rot = Mat4().InitIdentity();
		Mat4  z_rot = Mat4().InitIdentity();
		float x_sin = sinf(rotation.x), x_cos = cosf(rotation.x);
		float y_sin = sinf(rotation.y), y_cos = cosf(rotation.y);
		float z_sin = sinf(rotation.z), z_cos = cosf(rotation.z);

		x_rot.mat[5]  =  x_cos;
		x_rot.mat[6]  = -x_sin;
		x_rot.mat[9]  =  x_sin;
		x_rot.mat[10] =  x_cos;

		y_rot.mat[0]  =  y_cos;
		y_rot.mat[2]  = -y_sin;
		y_rot.mat[8]  =  y_sin;
		y_rot.mat[10] =  y_cos;

		z_rot.mat[0] =  z_cos;
		z_rot.mat[1] = -z_sin;
		z_rot.mat[4] =  z_sin;
		z_rot.mat[5] =  z_cos;

		mat = (z_rot * y_rot * x_rot).mat;

		return *this;
	}

	Mat4& Mat4::InitScale(const Vec3& scale)
	{
		InitIdentity();

		mat[0] = scale.x;
		mat[5] = scale.y;
		mat[10] = scale.z;
		
		return *this;
	}

	Mat4& Mat4::InitPerspective(float fov, float aspect, float near, float far)
	{
		float tan_half_fov = tanf(fov / 2.0f);
		float depth = near - far;

		mat[0] = 1.0f / (tan_half_fov * aspect);
		mat[5] = 1.0f / tan_half_fov;
		mat[10] = (-near - far) / depth;
		mat[11] = 2.0f * far * near / depth;
		mat[14] = 1.0f;

		return *this;
	}

	Mat4& Mat4::InitOrthographic(float left, float right, float bottom, float top, float near, float far)
	{
		float width = right - left;
		float height = top - bottom;
		float depth = far - near;

		InitIdentity();
		
		mat[0]  =  2.0f / width;
		mat[5]  =  2.0f / height;
		mat[10] = -2.0f / depth;
		mat[12] = -(right + left) / width;
		mat[13] = -(top + bottom) / height;
		mat[14] = -(far + near) / depth;

		return *this;
	}

	Mat4 operator*(const Mat4& lhs, const Mat4& rhs)
	{
		// Cache matrix values for extra speed
		float a00 = lhs.mat[0],  a01 = lhs.mat[1],  a02 = lhs.mat[2],  a03 = lhs.mat[3],
			  a10 = lhs.mat[4],  a11 = lhs.mat[5],  a12 = lhs.mat[6],  a13 = lhs.mat[7],
			  a20 = lhs.mat[8],  a21 = lhs.mat[9],  a22 = lhs.mat[10], a23 = lhs.mat[11],
			  a30 = lhs.mat[12], a31 = lhs.mat[13], a32 = lhs.mat[14], a33 = lhs.mat[15],
			  b00 = rhs.mat[0],  b01 = rhs.mat[1],  b02 = rhs.mat[2],  b03 = rhs.mat[3],
			  b10 = rhs.mat[4],  b11 = rhs.mat[5],  b12 = rhs.mat[6],  b13 = rhs.mat[7],
			  b20 = rhs.mat[8],  b21 = rhs.mat[9],  b22 = rhs.mat[10], b23 = rhs.mat[11],
			  b30 = rhs.mat[12], b31 = rhs.mat[13], b32 = rhs.mat[14], b33 = rhs.mat[15];

		return
		{{
			b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30,
			b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31,
			b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32,
			b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33,
			b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30,
			b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31,
			b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32,
			b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33,
			b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30,
			b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31,
			b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32,
			b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33,
			b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30,
			b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31,
			b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32,
			b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33
		}};
	}
}
