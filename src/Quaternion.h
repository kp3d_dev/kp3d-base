#pragma once

#include "Common.h"
#include "Vec3.h"

#include <ostream>

namespace kp3d
{
	class Vec3;

	class Quaternion
	{
	public:
		Quaternion();
		Quaternion(float x, float y, float z, float w);
		explicit Quaternion(float scalar);
		Quaternion(const Vec3& axis, float angle);

		float Length() const;
		float Dot(const Quaternion& other) const;
		Quaternion Conjugate() const;
		Quaternion Normalized() const;

		Quaternion& operator*=(const Quaternion& rhs);
		Quaternion& operator*=(const Vec3& rhs);

		Quaternion operator-() const;

		friend bool operator==(const Quaternion& lhs, const Quaternion& rhs);
		friend bool operator!=(const Quaternion& lhs, const Quaternion& rhs);
		friend bool operator< (const Quaternion& lhs, const Quaternion& rhs);
		friend bool operator<=(const Quaternion& lhs, const Quaternion& rhs);
		friend bool operator> (const Quaternion& lhs, const Quaternion& rhs);
		friend bool operator>=(const Quaternion& lhs, const Quaternion& rhs);

		friend Quaternion operator*(const Quaternion& lhs, const Quaternion& rhs);

		friend Quaternion operator*(const Quaternion& lhs, const Vec3& rhs);
		friend Quaternion operator*(const Vec3& lhs, const Quaternion& rhs);

		friend std::ostream& operator<<(std::ostream& lhs, const Quaternion& rhs);

	public:
		float x, y, z, w;

	};
}
