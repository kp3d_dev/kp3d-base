#pragma once

#include <math.h>

#include "Common.h"

namespace kp3d
{
	constexpr const float PI = 3.141593f;
	constexpr const float E = 2.718282f;

	constexpr const float RAD_TO_DEG = 57.295779f;
	constexpr const float DEG_TO_RAD = 0.017453f;

	KP3D_INLINE float ToDegrees(float radians)
	{
		return radians * RAD_TO_DEG;
	}

	KP3D_INLINE float ToRadians(float degrees)
	{
		return degrees * DEG_TO_RAD;
	}
}