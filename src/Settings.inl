#pragma once

#include <type_traits>
#include <sstream>

#include "Settings.h"

namespace kp3d
{
	/*
	 * This is a *horrible* way to go about this, but I'm not using Boost and MSVC
	 * doesn't support <charconv> yet which would be the ideal solution.
	 */
	template <typename T>
	auto Settings::Get(const std::string& key) const
	{
		std::string value = m_settings.at(key);

		if constexpr (std::is_same_v<T, int16> || std::is_same_v<T, int32> || std::is_same_v<T, int64>)
			return static_cast<T>(std::stoll(value));
		else if constexpr (std::is_same_v<T, uint16> || std::is_same_v<T, uint32> || std::is_same_v<T, uint64>)
			return static_cast<T>(std::stoull(value));
		else if constexpr (std::is_same_v<T, bool>)
			return value == "true";
		else if constexpr (std::is_same_v<T, double>)
			return std::stod(value);
		else if constexpr (std::is_same_v<T, float>)
			return std::stof(value);
		else if constexpr (std::is_same_v<T, std::string>)
			return value;
		else
			return ErrCode::FAILURE; // Should really consider actual error handling for this
	}
}
