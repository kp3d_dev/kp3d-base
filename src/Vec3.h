#pragma once

#include "Common.h"
#include "Quaternion.h"

#include <ostream>

namespace kp3d
{
	class Quaternion;

	class Vec3
	{
	public:
		Vec3();
		Vec3(float x, float y, float z);
		explicit Vec3(float scalar);
		explicit Vec3(const Quaternion& quaternion);

		float Length() const;
		float Dot(const Vec3& other) const;
		Vec3 Cross(const Vec3& other) const;
		Vec3 Normalized() const;
		Vec3 Rotated(const Vec3& axis, float degrees) const;
		Vec3 Rotated(const Quaternion& rotation) const;

		Vec3& operator+=(const Vec3& rhs);
		Vec3& operator+=(float rhs);
		Vec3& operator-=(const Vec3& rhs);
		Vec3& operator-=(float rhs);
		Vec3& operator*=(const Vec3& rhs);
		Vec3& operator*=(float rhs);
		Vec3& operator/=(const Vec3& rhs);
		Vec3& operator/=(float rhs);

		Vec3 operator-() const;

		friend bool operator==(const Vec3& lhs, const Vec3& rhs);
		friend bool operator!=(const Vec3& lhs, const Vec3& rhs);
		friend bool operator< (const Vec3& lhs, const Vec3& rhs);
		friend bool operator<=(const Vec3& lhs, const Vec3& rhs);
		friend bool operator> (const Vec3& lhs, const Vec3& rhs);
		friend bool operator>=(const Vec3& lhs, const Vec3& rhs);

		friend Vec3 operator+(const Vec3& lhs, const Vec3& rhs);
		friend Vec3 operator-(const Vec3& lhs, const Vec3& rhs);
		friend Vec3 operator*(const Vec3& lhs, const Vec3& rhs);
		friend Vec3 operator/(const Vec3& lhs, const Vec3& rhs);

		friend Vec3 operator+(const Vec3& lhs, float rhs);
		friend Vec3 operator+(float lhs, const Vec3& rhs);
		friend Vec3 operator-(const Vec3& lhs, float rhs);
		friend Vec3 operator-(float lhs, const Vec3& rhs);
		friend Vec3 operator*(const Vec3& lhs, float rhs);
		friend Vec3 operator*(float lhs, const Vec3& rhs);
		friend Vec3 operator/(const Vec3& lhs, float rhs);
		friend Vec3 operator/(float lhs, const Vec3& rhs);

		friend std::ostream& operator<<(std::ostream& lhs, const Vec3& rhs);

	public:
		float x, y, z;

	};
}
