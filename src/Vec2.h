#pragma once

#include "Common.h"

#include <ostream>

namespace kp3d
{
	class Vec2
	{
	public:
		Vec2();
		Vec2(float x, float y);
		explicit Vec2(float scalar);

		float Length() const;
		float Dot(const Vec2& other) const;
		float Cross(const Vec2& other) const;
		Vec2 Normalized() const;
		Vec2 Rotated(float degrees) const;

		Vec2& operator+=(const Vec2& rhs);
		Vec2& operator+=(float rhs);
		Vec2& operator-=(const Vec2& rhs);
		Vec2& operator-=(float rhs);
		Vec2& operator*=(const Vec2& rhs);
		Vec2& operator*=(float rhs);
		Vec2& operator/=(const Vec2& rhs);
		Vec2& operator/=(float rhs);

		Vec2 operator-() const;

		friend bool operator==(const Vec2& lhs, const Vec2& rhs);
		friend bool operator!=(const Vec2& lhs, const Vec2& rhs);
		friend bool operator< (const Vec2& lhs, const Vec2& rhs);
		friend bool operator<=(const Vec2& lhs, const Vec2& rhs);
		friend bool operator> (const Vec2& lhs, const Vec2& rhs);
		friend bool operator>=(const Vec2& lhs, const Vec2& rhs);

		friend Vec2 operator+(const Vec2& lhs, const Vec2& rhs);
		friend Vec2 operator-(const Vec2& lhs, const Vec2& rhs);
		friend Vec2 operator*(const Vec2& lhs, const Vec2& rhs);
		friend Vec2 operator/(const Vec2& lhs, const Vec2& rhs);

		friend Vec2 operator+(const Vec2& lhs, float rhs);
		friend Vec2 operator+(float lhs, const Vec2& rhs);
		friend Vec2 operator-(const Vec2& lhs, float rhs);
		friend Vec2 operator-(float lhs, const Vec2& rhs);
		friend Vec2 operator*(const Vec2& lhs, float rhs);
		friend Vec2 operator*(float lhs, const Vec2& rhs);
		friend Vec2 operator/(const Vec2& lhs, float rhs);
		friend Vec2 operator/(float lhs, const Vec2& rhs);

		friend std::ostream& operator<<(std::ostream& lhs, const Vec2& rhs);

	public:
		float x, y;

	};
}
