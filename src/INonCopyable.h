#pragma once

#include "Common.h"

namespace kp3d
{
	class INonCopyable
	{
	public:
		INonCopyable(const INonCopyable& other) = delete;
		const INonCopyable& operator=(const INonCopyable& other) = delete;

	protected:
		INonCopyable() = default;
		virtual ~INonCopyable() = default;

	};
}
