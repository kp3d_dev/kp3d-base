#include "Texture.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include <stb_image.h>

#include "LogSystem.h"

namespace kp3d
{
	Texture::Texture():
		m_gl_id(0),
		m_width(0),
		m_height(0)
	{
		glGenTextures(1, &m_gl_id);
	}

	Texture::~Texture()
	{
		glDeleteTextures(1, &m_gl_id);
	}

	ErrCode Texture::Load(const std::string& path)
	{
		int width = 0, height = 0;
		stbi_uc* data = stbi_load(path.c_str(), &width, &height, NULL, STBI_rgb_alpha);

		if (!data)
		{
			LogSystem::Instance().Log(LogLevel::WARNING, "Failed to load texture \"{}\": {}", path, stbi_failure_reason());

			return ErrCode::FAILURE;
		}

		ErrCode status = Load(data, width, height);

		stbi_image_free(data);

		return status;
	}

	ErrCode Texture::Load(const byte* data, uint width, uint height)
	{
		m_width = width;
		m_height = height;

		glBindTexture(GL_TEXTURE_2D, m_gl_id);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glBindTexture(GL_TEXTURE_2D, 0);

		return ErrCode::SUCCESS;
	}

	GLuint Texture::GLID() const
	{
		return m_gl_id;
	}
}
