#include "KP3D.h"

#include <stdlib.h>

#include <iostream>

#include <GL/glew.h>

namespace kp3d
{
	KP3D::KP3D(const std::string& config_path):
		m_sdl_window(nullptr, SDL_DestroyWindow),
		m_sdl_context(nullptr, SDL_GL_DeleteContext),
		m_running(true),
		m_game_config(config_path)
	{
		m_log_system.reset(new LogSystem(m_game_config.Get<std::string>("misc_log_path")));

		if (SDL_Init(SDL_INIT_EVERYTHING))
		{
			m_log_system->Log(LogLevel::ERROR, "Failed to initialize SDL: {}", SDL_GetError());
		}

		m_sdl_window.reset(
			SDL_CreateWindow(
				m_game_config.Get<std::string>("sdl_title").c_str(),
				SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
				m_game_config.Get<int>("sdl_width"),
				m_game_config.Get<int>("sdl_height"),
				SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN |
				static_cast<int>(m_game_config.Get<bool>("sdl_fullscreen"))
			)
		);
		if (!m_sdl_window)
		{
			m_log_system->Log(LogLevel::ERROR, "Failed to create SDL window: {}", SDL_GetError());
		}

		m_sdl_context.reset(SDL_GL_CreateContext(m_sdl_window.get()));
		if (!m_sdl_context)
		{
			m_log_system->Log(LogLevel::ERROR, "Failed to create GL context: {}", SDL_GetError());
		}

		glewExperimental = GL_TRUE;
		if (GLenum status = glewInit(); status != GLEW_OK)
		{
			m_log_system->Log(LogLevel::ERROR, "Failed to initialize GLEW: {}", glewGetErrorString(status));
		}

		m_ui_renderer.reset(new UIRenderer());
	}

	KP3D::~KP3D()
	{
		SDL_Quit();
	}

	void KP3D::Run()
	{
		// TODO: Actual game loop
		while (m_running)
		{
			SDL_Event e;
			while (SDL_PollEvent(&e))
			{
				if (e.type == SDL_QUIT)
					m_running = false;
			}

			Update();
			Render();

			SDL_GL_SwapWindow(m_sdl_window.get());
		}
	}

	//

	void KP3D::Update()
	{
		// ...
	}

	void KP3D::Render()
	{
		// Just testing, normally immediate mode is a bad idea
		glBegin(GL_TRIANGLES);
		glColor3f(1.0f, 0.0f, 0.0f), glVertex2f(-1.0f, -1.0f);
		glColor3f(0.0f, 1.0f, 0.0f), glVertex2f( 0.0f,  1.0f);
		glColor3f(0.0f, 0.0f, 1.0f), glVertex2f( 1.0f, -1.0f);
		glEnd();
	}
}

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cerr << "Usage: kp3d <config_path>\n";

		return EXIT_FAILURE;
	}

	kp3d::KP3D engine(argv[1]);
	engine.Run();

	return EXIT_SUCCESS;
}
